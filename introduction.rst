.. _introduction:

Introduction
=============
Your administration panel is available through the following domain: www.[your-domain].com/admin

Enter your username and password to Sign in. You should have received your username and password in your email.

.. image:: images/login.png
