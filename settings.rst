Settings
===========================================================
Users
**************************************************

Under the SETTINGS option of your admin menu you will have many options available. The good news is that you won’t have to handle most of these options. The two options that is useful for you to know how to handle are the USERS and the COLLECTIONS options.
Your administrator panel can have multiple users that can manage your website. In addition, you can create more accounts if needed from your admin menu.
To view and manage the admin user accounts of your website you need to click on the SETTINGS option in your admin panel.
Select the option USERS.

You will then be able to view all the user accounts.
When you hover your mouse over a USER’S NAME you can view the EDIT and DELETE buttons.
Use the DELETE button to delete an account.
Click on the EDIT button to edit an account.
To create a new account, click on the ADD A USER button and follow the steps to create the account.


Fill in the details in the fields.






Switch between TABS to select the role of the user you are creating. You can select different roles for your accounts and depending on the role you select you can grant different permissions e.g. administrators have full access to manage any aspect of the website.
Click SAVE to compete the process.
