.. Pixel Actions CMS Documentation documentation master file, created by
   sphinx-quickstart on Sun Aug  2 07:21:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pixel Actions CMS Documentation's documentation!
===========================================================

.. toctree::
   :maxdepth: 3

   introduction
   pages
   news
   settings
   cheatsheet
