Pages
=====
A **Page** is a publicly available page that is displayed on your website and a website can have multiple pages. Moreover, a page can have its own sub-pages called “CHILD PAGES”. A page that has child pages is also known as a “PARENT PAGE”.
Use your admin menu to navigate through the pages of your website. The first option of your menu is called “Pages” and from there you can view and access all the pages that are currently available on your website. The pages of your website follow a hierarchy based on their position in the menu. Your main page, which is called “Home”, represents the entire website and is the root of all your pages. From there you can move forward and find the page you are looking for, down the hierarchy of your pages.

Navigating through pages
**************************************************
1. Click on the **Pages** option in your admin menu.
2. Click on the FORWARD button at the end of your Home page tab to move further down the hierarchy of your website’s pages. The - FORWARD button on your page’s tab also indicates that the page has child pages.
3. Click on the EDIT button to enter the edit a page.
4. Click on the PAGE NAME to edit the child pages of the corresponding page.

.. image:: images/pages/page1.png
