
This is the page title Cheatsheet
=========================================
This is a Title
***************

This is a subititle
####################


* This is a bulleted list.
* It has two items, the second
  item uses two lines. (note the indentation)

1. This is a numbered list.
2. It has two items too.

Another:

#. This is a numbered list.
#. It has two items too.

.. csv-table:: a title
   :header: "name", "firstname", "age"
   :widths: 20, 20, 10

   "Smith", "John", 40
   "Smith", "John, Junior", 20

This is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph
, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph
, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph, this is a paragraph

.. image:: images/pages/page1.png


.. note::
   This is note text. Use a note for information you want the user to
   pay particular attention to.

.. warning::
  This is warning text. Use a warning for information the user must
  understand to avoid negative consequences.

Links to other sections:
*************************

Cross-References to Locations in the Same Document:
################################################################################

:ref:`This is a subititle`

Link to a section with custom text :ref:`link to a different section<This is a subititle>`.

References to Locations in Other Document:
################################################################################

Links to other pages
################################################################################
this is a link to another ref: introduction_

External Links
################################################################################

This is an external link:

Link Click here <https://domain.invalid/>
